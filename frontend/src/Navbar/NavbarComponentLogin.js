import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NavbarComponent = () => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg" fixed="top">
      <Navbar.Brand href="#home">&nbsp;&nbsp;
        <img
          alt="psu logo"
          src="https://www.fis.psu.ac.th/en/wp-content/uploads/2022/09/PSU-Logo-02.png"
          width="50"
          height="30"
          className="d-inline-block align-top"
        />{' '}
        Course Score
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          {/* เพิ่มเมนูเพิ่มเติมตามที่คุณต้องการ */}
        </Nav>
        {/* <Nav className="ml-auto">
          <Button variant="danger" onClick={handleLogout}>Logout</Button>
        </Nav> */}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarComponent;
