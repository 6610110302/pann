import React, { useState } from 'react';
import { Container, Row, Col, Form, Button, Alert, InputGroup } from 'react-bootstrap';
import axios from 'axios';
import axiosConfig from '../axios-interceptor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Icons from '@fortawesome/free-solid-svg-icons';
import NavbarComponentLogin from '../Navbar/NavbarComponentLogin';
import { Link, useNavigate } from 'react-router-dom';

function LoginPage() {
    const navigate = useNavigate()
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [submitEnabled, setSubmitEnabled] = useState(true);
    const [showAlert, setShowAlert] = useState(false);

    const handleUsernameChange = (e) => {
        setUsername(e.target.value);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSubmitEnabled(false);
        try {
            let result = await axios.post('http://localhost:1337/api/auth/local', {
                identifier: username,
                password: password
            })
            axiosConfig.jwt = result.data.jwt

            localStorage.setItem('jwtToken', result.data.jwt) //เก็บ jwt token

            result = await axios.get('http://localhost:1337/api/users/me?populate=role')

            if (result.data.role) {
                if (result.data.role.name === 'Student') {
                    navigate('/Pages/student');
                }
                else { //  ถ้าไม่ใช่นักศึกษามาหน้าของอาจารย์
                    navigate('/Pages/staff')
                }
            }
            console.log(result.data)
        } catch (e) {
            console.log(e)
            //console.log('wrong username & password')
            setSubmitEnabled(true);
            setShowAlert(true)
            console.log(showAlert)
        }
    };

    return (
        <div className="container-sm mt-5 rounded p-2">
            <NavbarComponentLogin />
            <Container className="mt-5">
                <p className="text-center fw-bolder">LOGIN FROM</p>
                <p className="text-center fw-normal">กรอกชื่อและรหัสเพื่อเข้าสู่ระบบ</p>
                <Row className="justify-content-center mb-3">
                    <Col xs={12} md={8}>
                        <Form onSubmit={handleSubmit} className="mt-3">
                            <Row className="g-3">
                                <Form.Group as={Col} md={6} controlId="validationCustomUsername">
                                    <Form.Label>Username</Form.Label>
                                    <InputGroup hasValidation>
                                        <InputGroup.Text id="inputGroupPrepend"><FontAwesomeIcon icon={Icons.faUser} /></InputGroup.Text>
                                        <Form.Control
                                            type="text"
                                            placeholder="Username"
                                            value={username}
                                            onChange={handleUsernameChange}
                                            aria-describedby="inputGroupPrepend"
                                            required
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            Please choose a username.
                                        </Form.Control.Feedback>
                                    </InputGroup>
                                    <Form.Text className="text-muted">
                                        กรอกชื่อของท่าน
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group as={Col} md={6} controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <InputGroup hasValidation>
                                        <InputGroup.Text id="inputGroupPrepend"><FontAwesomeIcon icon={Icons.faKey} /> </InputGroup.Text>
                                        <Form.Control
                                            type="password"
                                            placeholder="Password"
                                            value={password}
                                            onChange={handlePasswordChange}
                                            required
                                        />
                                    </InputGroup>
                                    <Form.Text className="text-muted">
                                        กรอกรหัสผ่านของท่าน
                                    </Form.Text>
                                </Form.Group>
                                <Col xs={12} md={7} className="mt-4 text-center text-md-start">
                                    <Button
                                        variant="dark"
                                        type="submit"
                                        onClick={handleSubmit}
                                        disabled={!submitEnabled}
                                        className="w-50"
                                    >
                                        Submit <FontAwesomeIcon icon={Icons.faArrowRightToBracket} />
                                    </Button>
                                </Col>
                                <Col md={6} className="mt=3">
                                    {showAlert && (
                                        <Alert variant="danger" onClose={() => setShowAlert(false)} dismissible>
                                            <Alert.Heading>Login Fail!</Alert.Heading>
                                            <p>Username หรือ Password ของคุณไม่ได้อยู่ในระบบ</p>
                                            <Link href="#">โปรดติดต่อผู้ดูแลระบบ</Link>
                                        </Alert>
                                    )}
                                </Col>

                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Container>

        </div>
    );
};

export default LoginPage;