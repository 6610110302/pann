import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Container, Card, ListGroup, Modal, Button, Badge } from 'react-bootstrap';
import moment from 'moment';
import NavbarComponent from '../Navbar/NavbarComponent';
import './design.css';


axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || "http://localhost:1337"
const URL_STUDENT = '/api/events/studentRelated'
const URL_GETUSER = '/api/users/me?populate=role'
const URL_EXCEL = '/api/excels'
const token = localStorage.getItem('jwtToken');

const StudentPage = () => {
  const [studentEventData, setStudentEventData] = useState([]); //Data - Event
  const [studentExcelData, setStudentExcelData] = useState([]); //Data - Excel

  /////////////// set Users //////////////////
  const [getUser, setGetUser] = useState();
  const [getEmail, setGetEmail] = useState();

  /////////////// set Modal //////////////////
  const [selectedEvent, setSelectedEvent] = useState(null);
  const [selectedExcel, setSelectedExcel] = useState(null);

  /////////////// Modal //////////////////
  const [showModalEvent, setShowModalEvent] = useState(false);
  const [showModalExcel, setShowModalExcel] = useState(false);


  //GET Data Event - Data Excel - Data User
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const result = await axios.get(URL_GETUSER)

        setGetUser(result.data.username)
        setGetEmail(result.data.email)
      } catch (error) {
        console.error('Error fetching User Data (Users):', error);
      }
    };
    const fetchStudentDataEvent = async () => {
      try {
        const response = await axios.get(URL_STUDENT, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setStudentEventData(response.data.data);
        //console.log(response.data.data)
      } catch (error) {
        console.error('Error fetching student (Event):', error);
      }
    };

    const fetchStudentDataExcel = async () => {
      try {
        const response = await axios.get(URL_EXCEL, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setStudentExcelData(response.data.data.filter(item => item.attributes.stdid === getUser));
      } catch (error) {
        console.error('Error fetching student data (Excel):', error);
      }
    };

    fetchStudentDataExcel();
    fetchStudentDataEvent();
    fetchUserData();
  }, [token, getUser]);


  const handleCardLinkClickEvent = (selectedEvent) => {
    setSelectedEvent(selectedEvent);
    setShowModalEvent(true);
  };

  const handleCardLinkClickExcel = (selectedExcel) => {
    setSelectedExcel(selectedExcel);
    setShowModalExcel(true);
  };

  const handleCloseModal = () => {
    setShowModalEvent(false);
    setShowModalExcel(false);
  };

  const noClick = (event) => {
    // ป้องกันการทำงานปกติของลิงค์
    event.preventDefault();
  };


  return (
    <div className="container-sm mt-5">
      <NavbarComponent onLogout={() => console.log('Logout')} />
      <br />
      <Container className="mt-5">
        <h1 className="text-center fw-bolder fontheadpage">Student Page</h1>
        <h4><p className="text-center">ประกาศคะแนนนักศึกษา</p></h4>
        <h6 className="text-center fontstudentpage-signed">
          Signed in as: <a class="link-underline-light" href="/" onClick={noClick}>{getUser}</a>
          &nbsp;
          Email in as: <a class="link-underline-light" href="/" onClick={noClick}>{getEmail}</a>
        </h6>
        <br /><br />
        <div className="row">
          <h3 className="text-left fw-bolder ">Score From Excel</h3>
          <hr></hr>
          {studentExcelData.map((excel) => (
            <div key={excel.id} className="col-md-3 mb-5">
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="https://img2.pic.in.th/pic/score.png" />
                <Card.Body>
                  <Card.Title>{excel.attributes.subject}</Card.Title>
                  <Card.Text>
                    <p><strong>ประกาศวันที่ :</strong> {moment(excel.attributes.date).format('YYYY-MM-DD HH:mm:ss')}</p>
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush ">
                  <ListGroup.Item><strong>คะแนน :</strong> {excel.attributes.score} </ListGroup.Item>
                  <ListGroup.Item><strong>สถานะ : </strong>
                    {excel.attributes.score >= 70 && <Badge bg="success">{excel.attributes.status}</Badge>}
                    {excel.attributes.score >= 50 && excel.attributes.score < 70 && <Badge bg="secondary">{excel.attributes.status}</Badge>}
                    {excel.attributes.score < 50 && <Badge bg="danger">{excel.attributes.status}</Badge>}
                  </ListGroup.Item>
                </ListGroup>
                <Card.Body>
                  <Card.Link href="#" onClick={() => handleCardLinkClickExcel(excel)}>
                    View Details
                  </Card.Link>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
        <div className="row">
          <h3 className="text-left fw-bolder">Score From Event</h3>
          <hr></hr>
          {studentEventData.map((event) => (
            <div key={event.id} className="col-md-3 mb-5">
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="https://img2.pic.in.th/pic/score.png" />
                <Card.Body>
                  <Card.Title>{event.attributes.name}</Card.Title>
                  <Card.Text>
                    <p><strong>ประกาศวันที่ :</strong> {moment(event.attributes.effective_datetime).format('YYYY-MM-DD HH:mm:ss')}</p>
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroup.Item><strong>คะแนน :</strong> {event.attributes.entry.result}
                  </ListGroup.Item>
                  <ListGroup.Item><strong>สถานะ : </strong>
                    {event.attributes.entry.result >= 70 && <Badge bg="success">Positive</Badge>}
                    {event.attributes.entry.result >= 50 && event.attributes.entry.result < 70 && <Badge bg="secondary">Natural</Badge>}
                    {event.attributes.entry.result < 50 && <Badge bg="danger">Negative</Badge>}
                  </ListGroup.Item>
                </ListGroup>
                <Card.Body>
                  <Card.Link href="#" onClick={() => handleCardLinkClickEvent(event)}>
                    View Details
                  </Card.Link>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      </Container>
      {/* Modal */}
      <Modal show={showModalEvent} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedEvent?.attributes.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p><strong>ประกาศวันที่ :</strong> {moment(selectedEvent?.attributes.effective_datetime).format('YYYY-MM-DD HH:mm:ss')}</p>
          <p><strong>คะแนน :</strong> {selectedEvent?.attributes.entry.result}</p>
          <p><strong>สถานะ : </strong>
            {selectedEvent?.attributes.entry.result >= 70 && <Badge bg="success">Positive</Badge>}
            {selectedEvent?.attributes.entry.result >= 50 && selectedEvent?.attributes.entry.result < 70 && <Badge bg="secondary">Natural</Badge>}
            {selectedEvent?.attributes.entry.result < 50 && <Badge bg="danger">Negative</Badge>}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      {/* ////////////////////////////////////////////// */}
      <Modal show={showModalExcel} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedExcel?.attributes.subject}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p><strong>ประกาศวันที่ :</strong> {moment(selectedExcel?.attributes.date).format('YYYY-MM-DD HH:mm:ss')}</p>
          <p><strong>คะแนน :</strong> {selectedExcel?.attributes.score}</p>
          <p><strong>สถานะ : </strong>
            {selectedExcel?.attributes.score >= 70 && <Badge bg="success">{selectedExcel?.attributes.status}</Badge>}
            {selectedExcel?.attributes.score >= 50 && selectedExcel?.attributes.score < 70 && <Badge bg="secondary">{selectedExcel?.attributes.status}</Badge>}
            {selectedExcel?.attributes.score < 50 && <Badge bg="danger">{selectedExcel?.attributes.status}</Badge>}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default StudentPage;
