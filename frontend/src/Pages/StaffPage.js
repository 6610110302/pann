import { useState, useEffect } from 'react';
import axios from 'axios';
import { Container, Table, Form, Button, Col, Row, Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faClock } from '@fortawesome/free-solid-svg-icons';
import DatePicker from 'react-datepicker';
import NavbarComponent from '../Navbar/NavbarComponent';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import * as XLSX from 'xlsx';
import './design.css';


axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || "http://localhost:1337"
const URL_EVENT = '/api/events'
const URL_GETUSER = '/api/users/me?populate=role'
const URL_EXCEL = '/api/excels'
const token = localStorage.getItem('jwtToken');

const StaffPage = () => {
    //State สำหรับเก็บข้อมูลฟอร์ม Event
    const [fromEventData, setFromEventData] = useState({
        name: '',
        effective_datatime: '',
        //owner: '',
        entries: '',
    });

    // State สำหรับเก็บข้อมูลที่จะแก้ไข Event
    const [editedEvent, setEditedEvent] = useState({
        id: null,
        name: '',
        effective_datetime: '',
    });
    const [showEditModal, setShowEditModal] = useState(false);


    //State เก็บข้อมูลที่ได้มาจาก API GET 
    const [eventData, setEventData] = useState([]);

    //State ลบประกาศ 
    const [deletedEventId, setDeletedEventId] = useState(null);

    //State เรียง ID จากมากไปน้อย 
    const sortedEventData = [...eventData].sort((a, b) => b.id - a.id);

    //State Upload File
    const [uploadedData, setUploadedData] = useState([]); // เพิ่ม state สำหรับเก็บข้อมูลที่อัปโหลด

    //State GET User
    const [getUser, setGetUser] = useState();
    const [getEmail, setGetEmail] = useState();

    //UseEffect สำหรับ GET ข้อมูลของ event มาแสดง || เก็ยข้อมูล User 
    useEffect(() => {
        const fetchEventData = async () => {
            try {
                const response = await axios.get(URL_EVENT, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                console.log(response.data.data)
                setEventData(response.data.data);

            } catch (error) {
                console.error('Error fetching events:', error);
            }
        };
        const fetchUserData = async () => {
            try {
                const result = await axios.get(URL_GETUSER)

                setGetUser(result.data.username)
                setGetEmail(result.data.email)

            } catch (error) {
                console.error('Error fetching User Data:', error);
            }
        };

        fetchUserData();
        fetchEventData();
    }, []);

    //UseEffect สำหรับการ Delete ข้อมูลรายการประกาศนั้นๆ
    useEffect(() => {
        const deleteEvent = async () => {
            try {
                await axios.delete(`${URL_EVENT}/${deletedEventId}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                setEventData(eventData.filter(event => event.id !== deletedEventId));
                setDeletedEventId(null);
            } catch (error) {
                console.error('Error deleting event:', error);
            }
        };

        if (deletedEventId !== null) {
            deleteEvent();
        }
    }, [deletedEventId, eventData]);

    const handleEdit = (eventId) => {
        const eventToEdit = eventData.find(event => event.id === eventId);
        if (eventToEdit) {
            setEditedEvent({
                id: eventToEdit.id,
                name: eventToEdit.attributes.name,
                effective_datetime: new Date(eventToEdit.attributes.effective_datetime),
            });
            setShowEditModal(true); // เปิด Modal
        }
    };

    const handleCancelEdit = () => {
        setEditedEvent({
            id: null,
            name: '',
            effective_datetime: '',
        });
        setShowEditModal(false); // ปิด Modal
    };


    const handleChange = (e) => {
        const { name, value } = e.target;
        setFromEventData({
            ...fromEventData,
            [name]: value,
        });
    };


    // Function สำหรับการบันทึกการแก้ไข
    const handleSaveEdit = async () => {
        try {
            const response = await axios.put(
                `${URL_EVENT}/${editedEvent.id}`,
                {
                    data: {
                        name: editedEvent.name,
                        effective_datetime: editedEvent.effective_datetime,
                    },
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            );

            if (response.data.data) {
                const updatedEvent = response.data.data;
                setEventData(prevEvents => {
                    return prevEvents.map(event =>
                        event.id === updatedEvent.id
                            ? {
                                ...event,
                                attributes: {
                                    ...event.attributes,
                                    name: updatedEvent.attributes.name,
                                    effective_datetime: updatedEvent.attributes.effective_datetime,
                                },
                            }
                            : event
                    );
                });
                handleCancelEdit(); // ยกเลิกการแก้ไขหลังจากบันทึกเรียบร้อย
            } else {
                console.error('Error updating item: Invalid response format');
            }
        } catch (error) {
            console.error('Error updating item:', error);
        }
    };

    const handleSubmitCreateEvent = async () => {
        try {
            const response = await axios.post(
                URL_EVENT,
                { data: fromEventData },
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            );

            if (response.data.data) {
                const { id, attributes } = response.data.data;

                // อัปเดต State ของข้อมูลที่ได้จาก API
                setEventData([
                    ...eventData,
                    {
                        id: id,
                        key: id,
                        ...attributes,
                    },
                ]);

                setFromEventData({
                    name: '',
                    effective_datetime: '',
                    owner: '',
                    entries: '',
                    // และข้อมูลอื่น ๆ ตามที่คุณต้องการ
                });

                // ไม่ต้องรีโหลดหน้าเว็บ
            } else {
                console.error('Error adding item: Invalid response format');
            }
        } catch (error) {
            console.error('Error adding item:', error);
        }
    };

    const handleDelete = (eventId) => {
        setDeletedEventId(eventId);
    };

    const handleFileUpload = (e) => {
        const reader = new FileReader();
        reader.readAsBinaryString(e.target.files[0]);
        reader.onload = (e) => {
            const data = e.target.result;
            const workbook = XLSX.read(data, { type: 'binary' });
            const sheetName = workbook.SheetNames[0];
            const sheet = workbook.Sheets[sheetName];
            const parsedData = XLSX.utils.sheet_to_json(sheet);
            setUploadedData(parsedData);
        };
    };

    const sendDataExcelToStrapi = async () => {
        try {
            for (const data of uploadedData) {
                const { stdid, score, subject, status } = data;

                const excelData = {
                    stdid: stdid.toString(), // format string
                    score: score.toString(), // format string
                    subject: subject,
                    status: status,
                    date: moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                };

                await axios.post(URL_EXCEL, { data: excelData }, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                });
            }

            setUploadedData([]);
            console.log('Data uploaded successfully');
        } catch (error) {
            console.error('Error sending data to Strapi:', error);
        }
    };


    const noClick = (event) => {
        // ป้องกันการทำงานปกติของลิงค์
        event.preventDefault();
    };


    return (
        <Container className="mt-3">
            <NavbarComponent onLogout={() => console.log('Logout')} />
            <br></br><br></br><br></br>
            <h2 className="text-center fw-bolder fontheadpage">Staff Page</h2>
            <h4><p className="text-center">สร้างประกาศคะแนนนักศึกษา</p></h4>
            <h6 className="text-center fontstudentpage-signed">
                Signed in as: <a class="link-underline-light" href="/" onClick={noClick}>{getUser}</a>
                &nbsp;
                Email in as: <a class="link-underline-light" href="/" onClick={noClick}>{getEmail}</a>
            </h6><br></br>
            <Row className="justify-content-center mb-3">
                <Col xs={12} md={9}>
                    <Container className="">
                        <Container className="bg-light p-4 rounded shadow">
                            <h2 className="text-center fw-bolder mb-4">Create Submission</h2>
                            <Form>
                                <Form.Group as={Col} md={12} controlId="validationCustomUsername">
                                    <Form.Label>
                                        <FontAwesomeIcon icon={faUser} /> Name of Event :
                                    </Form.Label>
                                    <Form.Control
                                        required
                                        type="text"
                                        name="name"
                                        value={fromEventData.name}
                                        onChange={handleChange}
                                    />
                                </Form.Group>
                                <br></br>
                                <Form.Group as={Col} md={12} controlId="Datetime">
                                    <Form.Label>
                                        <FontAwesomeIcon icon={faClock} /> Effective Datetime :
                                    </Form.Label>
                                    <div className="custom-datepicker">
                                        <DatePicker
                                            selected={fromEventData.effective_datetime}
                                            onChange={(date) => setFromEventData({ ...fromEventData, effective_datetime: date })}
                                            showTimeSelect
                                            dateFormat="yyyy-MM-dd HH:mm:ss"
                                            className="form-control"
                                        />
                                    </div>
                                </Form.Group>

                                <br></br>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <Button variant="primary" type="button" onClick={handleSubmitCreateEvent}>
                                    &nbsp;Submit
                                </Button>
                            </Form>
                        </Container>
                        <Container className="mt-5">
                            <Container className="bg-light p-4 rounded shadow">
                                <h2 className="text-center fw-bolder mb-4">Upload Score Excel File</h2>
                                <Form>
                                    <Form.Group className="mb-3">
                                        <Form.Label>
                                            <FontAwesomeIcon icon={faUser} /> Upload Excel File :
                                        </Form.Label>
                                        <Form.Control
                                            type="file"
                                            accept=".xlsx, .xls"
                                            onChange={handleFileUpload}
                                        />
                                    </Form.Group>
                                </Form>
                                {uploadedData.length > 0 && (
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                {Object.keys(uploadedData[0]).map((key) => (
                                                    <th key={key}>{key}</th>
                                                ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {uploadedData.map((row, index) => (
                                                <tr key={index}>
                                                    {Object.values(row).map((value, index) => (
                                                        <td key={index}>{value}</td>
                                                    ))}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                )}
                                <Button variant="primary" type="button" onClick={sendDataExcelToStrapi}>
                                    Confirm File
                                </Button>
                            </Container>
                        </Container>
                        <Container className="mt-5">
                            <h2 className="text-center fw-bolder mb-4">ประกาศคะแนนนักศึกษา</h2>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Effective Datetime</th>
                                        <th>Action</th>
                                        {/* <th>Owner</th>
                                <th>Entries</th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {sortedEventData.map((event) => (
                                        <tr key={event.id}>
                                            <td>{event.id}</td>
                                            <td>{event.attributes.name}</td>
                                            <td>{moment(event.attributes.effective_datetime).format('YYYY-MM-DD HH:mm:ss')}</td>
                                            <td>
                                                <Button variant="primary" onClick={() => handleEdit(event.id)}>
                                                    Edit
                                                </Button>
                                                <Button variant="danger" onClick={() => handleDelete(event.id)}>
                                                    Delete
                                                </Button>
                                            </td>
                                            {/*  <td>{event.attributes.owner}</td>
                                    <td>{event.attributes.entries}</td> */}
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Container>
                    </Container>
                    <Container className="mt-5">
                        <Modal show={showEditModal} onHide={handleCancelEdit}>
                            <Modal.Header closeButton>
                                <Modal.Title>Edit Event</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group as={Col} md={12} controlId="editEventName">
                                        <Form.Label>
                                            <FontAwesomeIcon icon={faUser} /> Name of Event :
                                        </Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="name"
                                            value={editedEvent.name}
                                            onChange={(e) => setEditedEvent({ ...editedEvent, name: e.target.value })}
                                        />
                                    </Form.Group>
                                    <br></br>
                                    <Form.Group as={Col} md={12} controlId="editEventDatetime">
                                        <Form.Label>
                                            <FontAwesomeIcon icon={faClock} /> Effective Datetime :
                                        </Form.Label>
                                        <br></br>
                                            <DatePicker
                                                selected={editedEvent.effective_datetime}
                                                onChange={(date) => setEditedEvent({ ...editedEvent, effective_datetime: date })}
                                                showTimeSelect
                                                dateFormat="yyyy-MM-dd HH:mm:ss"
                                                className="form-control"
                                            />
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="primary" onClick={handleSaveEdit}>
                                    Save
                                </Button>
                                <Button variant="secondary" onClick={handleCancelEdit}>
                                    Cancel
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </Container>
                </Col>
            </Row>
        </Container>

    );
}

export default StaffPage;